// GNU Affero General Public License V.3.0 or AGPL-3.0
//
// amazon bookstore remake SERVER
// Copyright (C) 2021,2022 - quoeamaster@gmail.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

var fs = require('fs');

let esUser = '';
let esPwd = '';
let esUrl = '';
let serverSecret = '';

// [replace] replace the secret-prod.md to your updated credential file instead
const _content = fs.readFileSync('./secret.md', 'utf-8');
const _lines = _content.split('\n');
_lines.forEach((_line, _idx, _) => {
   if (!_line.startsWith('#')) {
      if (_line.startsWith('es.user:')) {
         esUser = _line.substring(8);
      } else if (_line.startsWith('es.pwd:')) {
         esPwd = _line.substring(7);
      } else if (_line.startsWith('es.url:')) {
         esUrl = _line.substring(7);
      } else if (_line.startsWith('server.secret:')) {
         serverSecret = _line.substring(14);
      }
   }
});
// [debug]
//console.log(`${esUser} - ${esPwd} - ${esUrl} => ${serverSecret} `);

module.exports = {
   ES_URL: esUrl,
   ES_USER: esUser,
   ES_PWD: esPwd,
   SERVER_SECRET: serverSecret,
};
