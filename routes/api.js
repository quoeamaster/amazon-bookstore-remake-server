// GNU Affero General Public License V.3.0 or AGPL-3.0
//
// amazon bookstore remake SERVER
// Copyright (C) 2021,2022 - quoeamaster@gmail.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

var express = require('express');
var router = express.Router();
var debug = require('debug')('amazon-bookstore-remake-server:server');
var bcrypt = require('bcrypt');

// where the secrets are stored.
var cf = require('./../ConfigFile.js');
var es = require('./../ESWrapper.js');
var jwt = require('./../JWTWrapper.js');
var constants = require('./../Constants.js');
var authMiddlewre = require('./../AuthPlugin.js');

// config the ESearch wrapper
es.connector.Config({ node: cf.ES_URL, username: cf.ES_USER, password: cf.ES_PWD });
const _esClient = es.connector.Connect();
// [debug] try to run a must ok query
//_testESConnect();

/**
 * [test|debug]
 * _testESConnect
 * a tester method to check ES connectivity issues.
 */
async function _testESConnect() {
  console.log('#### run a general meta query on ES cluster:');
  const _info = await _esClient.info();
  console.log(_info);
 
  // [run a normal es query]
  console.log('#### run a query against .kibana');
  const _testQResult = await _esClient.search({
    index: '.kibana',
  });
  //debug(_testQResult);
  debug(_testQResult['hits']['hits']);
}

/**
 * [POST] login 
 * 
 * return { code: 200|500, isAuthOk: true|false, errMsg }
 */
router.post('/login', async function(req, res, next) {
  // retrieve the user/pwd pair
  const { username, password } = req.body;
  // [debug]
  //debug(`user:${username} / pwd:${password}`);

  await (async function _esGetByUsername() {
    const _expiryInString = '24h'; // 1m for testing
    try {
      // query for the user based on username and verify the password-hashed
      // [ref] https://www.elastic.co/guide/en/elasticsearch/client/javascript-api/8.0/get_examples.html
      const _doc = await _esClient.get({
        "index": constants.ES_BOOKSHOP_USERS_INDEX,
        "id": username,
      });
      // check the hashed password
      const _matched = await bcrypt.compare(password, _doc['_source']['password']);
      if (false == _matched) {
        throw new Error(`username exists BUT password is incorrect`)
      }
      // GREAT~ authenticated... create JWT Token
      const _token = jwt.sign({
        username: username, // [missing] authorization rights if any...
      }, null, _expiryInString);
      
      if (null == _token.errMsg) {
        // jwt created successfully
        // write a httpOnly cookie providing the token
        // [ref] https://stackoverflow.com/questions/71062324/httponly-cookies-are-not-sent-with-request-to-server
        // [ref] https://web.dev/samesite-cookies-explained/
        res.cookie(constants.U_TOKEN, _token.token, { 
          httpOnly: true, 
          maxAge: jwt.getExpireInMS(_expiryInString),
          origin: 'http://localhost:3001',
          // check the values 
          // - none (https sites only, also need to add 'Secure' attribute), 
          // - strict (only ok when linked from 1st-party website - same domain:port) and 
          // - lax (as long as valid, all cookies would be sent to the server, even under following link / sites)
          sameSite: 'lax', 
        });
        res.send({
          'code': 200,
          isAuthOk: true,   // meaning authenticated correctly
        });
      } else {
        // [lesson] if sign error, treat it as status 200, 
        // if not the actual message would be absorbed by axios / browser(??)
        res.send({
          'code': 500,
          isAuthOk: false,        // meaning authenticated failed
          errMsg: _token.errMsg,  // error message
        });
      }
    } catch(e) {
      const _eRes = es.connector.ErrorHandler(e, { 'api': '/api/login', 'username': username, })
      res.send(_eRes);
    }
  })();
});

/**
 * [GET] search
 * 
 * return { code: 200|500, payload: JSON, errMsg: ERR_MSG }
 */
router.get('/search', async function(req, res, next) {
  // TODO: searchAfter is required for pagination purpose; so the client side MUST keep track of the last doc's sorting_values.
  const { shouldShowAdvDlg, queryText, advFields, advRangeFields, advRangeStart, advRangeEnd, from, size, searchAfter, } = req.query;
  try {
    // build the query
    let _q = {};
    // [lesson] the queryString params are all 'string' and no smart logic to data cast; hence 'true' instead of true.
    if ('true' == shouldShowAdvDlg || true == shouldShowAdvDlg) {
      let _msearchQ = {
        "query": queryText,
        "fields": advFields
      };
      // avoid empty queryText
      if (queryText.trim() == '') {
        delete _msearchQ['query'];
      }
      // build the range query
      let _rangeQ = {};
      let _rangeQAttrs = {};
      if ('num_pages' == advRangeFields || 'average_rating' == advRangeFields || 'ratings_count' == advRangeFields) {
        // parse to int
        const _nStart = Number(advRangeStart);
        const _nEnd = Number(advRangeEnd);
        if (null != advRangeStart && advRangeStart.trim() != '' && Number.isInteger(_nStart)) {
          _rangeQAttrs['gte'] = parseInt(advRangeStart, 10);
        }
        if (null != advRangeEnd && advRangeEnd.trim() != '' && Number.isInteger(_nEnd)) {
          _rangeQAttrs['lte'] = parseInt(advRangeEnd, 10);
        }
      } else if ('@timestamp' == advRangeFields) {
        // no checks on date-format (good luck...)
        if (null != advRangeStart && advRangeStart.trim() != '') {
          _rangeQAttrs['gte'] = advRangeStart;
        }
        if (null != advRangeEnd && advRangeEnd.trim() != '') {
          _rangeQAttrs['lte'] = advRangeEnd;
        }
      }
      _rangeQ[advRangeFields] = _rangeQAttrs;
      
      _q = {
        "bool": {
          "must": [
            {
              "multi_match": _msearchQ
            }, 
            {
              "range": _rangeQ
            }
          ]
        }
      };
    } else {
      _q = {
        "multi_match": {
          "query": queryText,
          "fields": [
            "title", 
            "authors",
            "publisher"
          ]
        }
      };
      // avoid empty queryText
      if (queryText.trim() == '') {
        delete _q['multi_search']['query'];
      }
    }
    
    // build the query-request
    let _qReq = {
      index: constants.ES_BOOKSHOP_INDEX,
      // [debug]
      //index: constants.ES_BOOKSHOP_INDEX+"12",
      query: _q,
      size,
      from,
      track_total_hits: true,
      // sorting is a MUST to make sure search_after possible (exceed the 10,000 docs limit)
      sort: [ { "_score": "desc" },
        { "_doc": "asc" } ],
    }
    const _needSearchAfter = (null != searchAfter && Object.keys(searchAfter).length > 0);
    if (true == _needSearchAfter) {
      _qReq['search_after'] = searchAfter;
    }
    // execute the search against ESearch
    //debug(JSON.stringify(_qReq));
    //debug(`### done with the qReq`);

    const _hits = await _esClient.search(_qReq); // end - es query
    //debug(_hits);
    //debug(`### done with the results`);

    res.send({
      code: 200, 
      payload: _hits,
    });

  } catch(e) {
    // [lesson] for all es related error ... must have a property "name" <- check this before any parsing etc
    //debug(`${e} \n\r${Object.keys(e)} -> ${e.name} => ${e.meta}\r\n${Object.keys(e.meta)}`);
    const _eRes = es.connector.ErrorHandler(e, { 'api': '/api/search', 'queryText': queryText, })
    res.send(_eRes);
  }
});

/**
 * [GET] searchForLanding
 * 
 * return { code: 200:500, payload: JSON, errMsg: ERR_MSG }
 */
router.get('/searchForLanding', async function(req, res, next) {
  try {
    let _qReq = {
      searches: [
        { "index": constants.ES_BOOKSHOP_INDEX },
        {"size":5,"sort":[{"average_rating":"desc"},{"ratings_count":"desc"}]},
        { "index": constants.ES_BOOKSHOP_INDEX },
        {"size":5,"sort":[{"ratings_count":"desc"},{"average_rating":"desc"}]},
      ],
    };
    // return _hits => { responses: [ { hits.hits: [] }, { hits.hits: [] } ] }
    const _hits = await _esClient.msearch(_qReq); // end - es query
    res.send({
      code: 200, 
      payload: _hits,
    });
  } catch(e) {
    const _eRes = es.connector.ErrorHandler(e, { 'api': '/api/searchForLanding' })
    res.send(_eRes);
  }
});

/**
 * [GET] get
 * 
 * return { code: 200:500, payload: JSON, errMsg: ERR_MSG }
 */
router.get('/get', async function(req, res, next) {
  try {
    const { docID } = req.query;
    let _req = {
      'index': constants.ES_BOOKSHOP_INDEX,
      'id': docID,
    };
    const _hit = await _esClient.get(_req);
    res.send({
      code: 200, 
      payload: _hit,
    });

  } catch(e) {
    const _eRes = es.connector.ErrorHandler(e, { 'api': '/api/get' })
    res.send(_eRes);
  }
});

router.post('/addToCart', authMiddlewre, async function(req, res, next) {
  try {
    const { username, docID, isbn, isbn13, title, bookID, publisher, authors } = req.body;
    // [debug]
    //debug(`params: => ${username}`);
    // retrieve the original cart document
    let _res = await _esClient.get({
      "index": constants.ES_BOOKSHOP_CART_INDEX,
      "id": username,
    }, {
      // [lesson] if http status is 404, not treat it as error; 
      // as it doesn't make sense to treat it as error in here...
      ignore: [404]
    });
    // cart document available??
    let _doc = {};
    const _isDocAvail = (_res['found'] == true);

    // validate and append the new item to the cart's item array
    if (true == _isDocAvail) {
      _doc = _res['_source'];
    } else {
      _doc['created_at'] = (new Date()).toISOString();
      _doc['items'] = [];
    }
    _doc['last_updated_at'] = (new Date()).toISOString();

    let _isDuplicateBookID = false;
    if (true == _isDocAvail) {
      _doc.items.forEach(item => {
        if (false == _isDuplicateBookID && item.bookID == bookID) {
          _isDuplicateBookID = true;
        }
      });
    }
    if (false == _isDuplicateBookID) {
      _doc.items.push({
        isbn,
        isbn13,
        title,
        bookID,
        publisher,
        authors,
        docID,
      });
    }

    // update the cart document
    _res = await _esClient.index({
      index: constants.ES_BOOKSHOP_CART_INDEX,
      id: username,
      document: _doc,
    });
    res.send({
      code: 200, 
      payload: _res,
    });
    
  } catch(e) {
    const _eRes = es.connector.ErrorHandler(e, { 'api': '/api/addToCart' })
    res.send(_eRes);
  }
});

/**
 * [POST] logout
 */
router.post('/logout', async function(req, res, next) {
  res.clearCookie(constants.U_TOKEN, { 
    httpOnly: true, 
    origin: 'http://localhost:3001',
    // check the values 
    // - none (https sites only, also need to add 'Secure' attribute), 
    // - strict (only ok when linked from 1st-party website - same domain:port) and 
    // - lax (as long as valid, all cookies would be sent to the server, even under following link / sites)
    sameSite: 'lax', 
  });
  res.send({
    code: 200, 
    payload: `successfully logout`,
  });
});

/**
 * [GET] cart
 * 
 * return the shopping cart content based on username.
 */
router.get('/cart', authMiddlewre, async function(req, res, next) {
  try {
    const { username } = req.query;
    const _doc = await _esClient.get({
      "index": constants.ES_BOOKSHOP_CART_INDEX,
      "id": username,
    });
    res.send({
      code: 200, 
      payload: _doc,
    });

  } catch(e) {
    const _eRes = es.connector.ErrorHandler(e, { 'api': '/api/cart' })
    res.send(_eRes);
  }
});

/**
 * [POST] checkout
 * 
 * checkout the cart based on [username], should also move the order as a history in another data index though...
 */
router.post('/checkout', authMiddlewre, async function(req, res, next) {
  try {
    const { username } = req.body;
    // [debug]
    //console.log(`${username} received`)
    // update the "items" field back to an empty []
    const _res = await _esClient.update({
      "index": constants.ES_BOOKSHOP_CART_INDEX,
      "id": username, 
      "doc": {
        items: [],
        "last_updated_at": (new Date()).toISOString(),
      },
    });
    res.send({
      code: 200, 
      payload: _res,
    });
  } catch(e) {
    const _eRes = es.connector.ErrorHandler(e, { 'api': '/api/checkout' })
    res.send(_eRes);
  }
});


// [test|debug]
// test route with AuthPlugin added as part of the handler
router.get('/testJwtToken', authMiddlewre, async function(req, res, next) {
  res.send({
    code: 200, 
    message: 'EVERYthing is FINE, passed authentication check~',
  });
});

module.exports = router;

