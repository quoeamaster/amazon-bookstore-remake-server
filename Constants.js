// GNU Affero General Public License V.3.0 or AGPL-3.0
//
// amazon bookstore remake SERVER
// Copyright (C) 2021,2022 - quoeamaster@gmail.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

module.exports = {
   // U_TOKEN - key associated with the signed jwt token's
   U_TOKEN: "u_token_jwt",

   ES_BOOKSHOP_INDEX: 'bookshop',
   ES_BOOKSHOP_USERS_INDEX: 'bookshop_users',
   ES_BOOKSHOP_CART_INDEX: 'bookshop_cart',

};