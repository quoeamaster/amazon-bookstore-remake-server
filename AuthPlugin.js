// GNU Affero General Public License V.3.0 or AGPL-3.0
//
// amazon bookstore remake SERVER
// Copyright (C) 2021,2022 - quoeamaster@gmail.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

const constants = require('./Constants.js');
const jwt = require('./JWTWrapper.js');
const cf = require('./ConfigFile.js');
const debug = require('debug')('amazon-bookstore-remake-server:server');

module.exports = async function(req, res, next) {
   try {
      const _cookieJwt = req.cookies[constants.U_TOKEN];
      const _res = jwt.verify(_cookieJwt, cf.SERVER_SECRET);
      // [debug]
      //console.log(`${cf.SERVER_SECRET} // ${_cookieJwt}`);
      
      // error - is jwt provided?
      if (null != _res.errMsg) {
         if (_res.errMsg.indexOf('jwt must be provided') != -1) {
            // [lesson] if status is non 200; this message would be absorbed (bug??) 
            //    and instead return a "request failed with status code 401"; hence still treat it as status 200
            res.status(200).send({
               code: 401,
               errMsg: `[unauthorized] ${_res.errMsg}. Please login again`,
            });
         } else {
            debug(`[AuthPlugin][unknown-error] ${_res.errMsg}`);
            // [lesson] for unknown reasons, might be ok to set status as 500 directly
            res.status(500).send({
               code: 500,
               errMsg: `[unknown error] ${_res.errMsg}`,
            });
         } // end if - error type
      } else {
         // [lesson] sample response that succeeded
         // { 'success': true, 'decoded': { 'username': 'USER', 'iat': epoch_xxx, 'exp': epoch_yyy } }
         /* { 
             success: true,
             decoded: { username: 'john.loo', iat: 1657856877, exp: 1657856937 }
            }
         */
         next();
      }
   } catch(e) {
      debug(`[AuthPlugin][general-catch] ... `)
      debug(e);
      
      const _status = e.response.status;
      res.status(_status).send({
         code: _status,
         errMsg: e.message,
      });
   }
};