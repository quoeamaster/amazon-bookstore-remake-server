// GNU Affero General Public License V.3.0 or AGPL-3.0
//
// amazon bookstore remake SERVER
// Copyright (C) 2021,2022 - quoeamaster@gmail.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

const { Client } = require('@elastic/elasticsearch');
const fs = require('fs');

// _esConnector is a Singleton wrapping an ES Client object.
// [feature] missing a connection pool here...
let _esConnector = (() => {   
   return {
      // Config - set the config metadata for connecting to ESearch.
      Config: function(config) {
         if (this._config == null) {
            this._config = config;
         }
      },
      // Reset - a way to reset the config meta-data and hence able to re-connect to ESearch.
      Reset: function() {
         this._config = {};
         this._instance = null;
      },
      // ErrorHandler - generic error handler for ES-related responses
      ErrorHandler: function(e, payload) {
         // check what error...
         // [debug]
         //console.log(e);
         const _eString = `${e}`;
         const _api = payload['api'];
         if (_eString.indexOf('ResponseError') != -1) {
           // not found case?
           if (_eString.indexOf('"found":false') != -1) {
             return {
               'code': 500,
               'message': `[${_api}] username [${payload['username']}] not found`,
             };
           }
         }
         // return an error json message [the remaining cases]
         return{
            'code': 500,
            'message': `[${_api}] ${e}`,
         };
      },
      // Connect - create the singleton instance through 
      // using the meta-data provided through the instance variable "config".
      Connect: function() {
         if (this._instance == null) {
            // node (url), username and password MUST be provided.
            const { node, username, password } = this._config;
            // [debug]
            //console.log(`** config: ${node} - ${username} vs ${password}`);

            // create ES client
            this._instance = new Client({
               node: node,
               auth: {
                  username: username,
                  password: password,
               },
               tls: {
                  ca: fs.readFileSync('./http_ca.crt'),
                  rejectUnauthorized: false,
               },
            });
         }
         return this._instance;
      },
   };
})();

// return the _esConnector object.
module.exports = {
   connector: _esConnector,
}