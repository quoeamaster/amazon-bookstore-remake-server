// GNU Affero General Public License V.3.0 or AGPL-3.0
//
// amazon bookstore remake SERVER
// Copyright (C) 2021,2022 - quoeamaster@gmail.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

// [deprecated] we don't need index and users routes
//var indexRouter = require('./routes/index');
//var usersRouter = require('./routes/users');
var apiRouter = require('./routes/api');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

// set CORS (plugin actually)
app.use((_, res, next) => {
  // allow-origin to access this server -> http://localhost:3000 or anything '*'
  // if "*" is set, then application logics should be added to filter out 
  // which request domains should be served and which should be ignored. 
  //
  // example: https://my-bank:9500 should be acceptable; however http://my-bank:9500 should NOT.
  // this approach is much more flexible compared with hard-coding 
  // the Access-Control-Allow-Origin header...
  res.setHeader('Access-Control-Allow-Origin', 'http://localhost:3000');
  // allow-headers from the request side -> 
  // Origin, X-Requested-With, Content, Accept, Content-Type, Authorization
  res.setHeader('Access-Control-Allow-Headers', 
    'Origin, X-Requested-With, Content, Accept, Content-Type, Authorization');
  // allow-methods -> everything GET, POST, PUT, DELETE, PATCH, OPTIONS
  res.setHeader('Access-Control-Allow-Methods', 
    'GET, POST, OPTIONS');
  // allow-credentials -> tell the browsers to expose the response to 
  // front-end JavaScript code when the request's credentials mode Request
  res.setHeader('Access-Control-Allow-Credentials', 
    true);

  next();
});

// [deprecated] only required api path and not "/" or "/users"
//app.use('/', indexRouter);
//app.use('/users', usersRouter);
app.use('/api', apiRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
