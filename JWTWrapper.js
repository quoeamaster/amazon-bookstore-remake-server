// GNU Affero General Public License V.3.0 or AGPL-3.0
//
// amazon bookstore remake SERVER
// Copyright (C) 2021,2022 - quoeamaster@gmail.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

var jwt = require('jsonwebtoken');
var cf = require('./ConfigFile.js'); // where the secrets are stored.

// [ref] https://thewebdev.info/2022/03/05/how-to-check-if-token-expired-using-the-node-js-jwt-library/
// [ref] https://stackoverflow.com/questions/51292406/check-if-token-expired-using-this-jwt-library

module.exports = {
   // sign - sign and create a JWT token.
   // return { token, errMsg }
   sign: function(payload, secret, expiry) {
      const _secret = secret || cf.SERVER_SECRET;  // defaults to the server_secret provided
      const _expiry = expiry || '24h';             // defaults to 1 day expiry
      let _res = {};
      try {
         const _token = jwt.sign(payload, _secret, {
            expiresIn: _expiry,
         });
         _res['token'] = _token;

      } catch(e) {
         // return null as token and provide an error message
         _res['errMsg'] = `${e}`;
      }
      return _res;
   },

   // verify - verify the provided token and checks expiry etc
   // return { success, errMsg, decoded }
   verify: function(jToken, secret) {
      const _secret = secret || cf.SERVER_SECRET;
      let _res = {};
      try {
         const _decodedToken = jwt.verify(jToken, _secret);
         // [debug]
         //console.log(`### decoded token (should be an object)`);
         //console.log(_decodedToken);
         _res['success'] = true;
         _res['decoded'] = _decodedToken;

      } catch(e) {
         _res['errMsg'] = `${e}`;
      }
      return _res;
   },

   // getExpireInMS - returns the expiry-time in MS. Only supports "d - day", "h - hour" and "m - minute".
   // 1 min          = 60000 ms
   // 1 hour         = 3600000 ms
   // 1 day          = 86400000 ms
   // example: 3d2h  = 266400000 ms
   getExpireInMS: function(expiry) {
      const MIN_MS   = 60000;
      const HOUR_MS  = 3600000;
      const DAY_MS   = 86400000;
      const U_MIN    = "m";
      const U_HOUR   = "h";
      const U_DAY    = "d";

      let _msTotal = 0;
      let _part = '';
      for (let i=0; i<expiry.length; i++) {
         const _char = expiry[i];
         // is a number~
         if (!isNaN(_char)) {
            _part += _char;
         } else {
            // is it a valid unit?
            switch (_char) {
               case U_MIN:
                  _msTotal += parseInt(_part) * MIN_MS;
                  _part = ''; // reset
                  break;
               case U_HOUR:
                  _msTotal += parseInt(_part) * HOUR_MS;
                  _part = ''; // reset
                  break;
               case U_DAY:
                  _msTotal += parseInt(_part) * DAY_MS;
                  _part = ''; // reset
                  break;
            }
         }
      } // for (expiry char loop)
      return _msTotal;
   },

};
